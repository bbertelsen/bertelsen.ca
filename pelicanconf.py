#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = u'Brandon Bertelsen'
SITENAME = u'Bertelsen'
SITEURL = 'http://bertelsen.ca'
FEED_DOMAIN = 'http://bertelsen.ca'


TIMEZONE = 'America/Toronto'

DEFAULT_LANG = u'en'

LINKS = (('@resume','http://linkedin.com/in/bbertelsen'),
         ('@stackoverflow','http://stackoverflow.com/users/170352/brandon-bertelsen'),
         ('@credo','http://www.credo.ca'),
         ('@locksmith','http://locks.bertelsen.ca'),

         )

DELETE_OUTPUT_DIRECTORY = True
STATIC_PATHS = ['assets',]
FILES_TO_COPY = (('assets/.htaccess', '.htaccess'),)


DEFAULT_PAGINATION = 20
THEME = "/home/brandon/Dropbox/workspace/web/bertelsen.ca/theme/"
GOOGLE_ANALYTICS = "UA-877651-1"

ARTICLE_URL = '{category}/{slug}/'
ARTICLE_SAVE_AS = '{category}/{slug}/index.html'
CATEGORY_URL = '{slug}'
CATEGORY_SAVE_AS = '{slug}/index.html'
PAGE_DIR = '/home/brandon/Dropbox/workspace/web/bertelsen.ca/content/pages/'
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'
DISPLAY_PAGES_ON_MENU = True
