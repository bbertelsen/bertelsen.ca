Title: Embracing Javascript
Date: 2013-12-14
Tags: javascript, json, nodejs, mongodb
category: programming
Summary: A few notes about javascript, json, mongodb, node and related frameworks.
toc: True

# Javascript

> A concise and balanced mix of principles and pragmatics. I loved the tutorial-style game-like program development. This book rekindled my earliest joys of programming. Plus JavaScript! —Brendan Eich, the man who gave us JavaScript -- [Eloquent Javascript: A Modern Introduction to Programming by Marijn Haverbeke](http://eloquentjavascript.net/contents.html)
    
## JSON

Pretty print JSON data: 

    :::bash
    echo file.json | python mjson.tool

# Node

## Installing Node from Source on Ubuntu

    :::bash
    cd ~
    mkdir src && cd src
    wget -N http://nodejs.org/dist/node-latest.tar.gz
    tar xzvf node-latest.tar.gz && cd node-v*
    # when it asks if you want to change anything remove "v" from the version number
    ./configure
    sudo checkinstall
    sudo dpkg -i node_*  

## Basic Introduction

> There is lots of information about node.js, but given the rapid pace at which it is developing, it can be difficult for beginners to find good, current information on how to get started. This guide aims to provide exactly that, whilst staying updated with the latest stable version of node.js. -- [Felix's Node.js Beginners Guide](http://nodeguide.com/beginner.html)
    
    :::javascript
    var http = require('http');
    
    var server = http.createServer(function(req, res) {
    res.writeHead(200);
    res.end('Hello Http');
    });
    server.listen(8080);

* Node provides a fairly simple module system. `require('./filename')` pulls in a file in the same directory as a variable whose functions can then be accessed in a manner similar to python by using `filename.functionname();`
* `EventEmitters` follow the [observer pattern](http://en.wikipedia.org/wiki/Observer_pattern) allowing you to capture changes by treating the emitters as dependents.

## Using Sails

> Sails.js make it easy to build custom, enterprise-grade Node.js apps. It is designed to mimic the MVC pattern of frameworks like Ruby on Rails, but with support for the requirements of modern apps: data-driven APIs with scalable, service-oriented architecture.

    :::bash
    sudo npm -g install 
    sails new testProject
    cd testProject
    sails lift
    # navigate to http://localhost:1337

## MongoDB Notes

* Stores JSON data rather than tabular data as is typical in SQL databases.
* Stores data as collections, documents, fields. Fields can be made up of all of the types you would usually expect
* MongoDB Automatically includes unique key for each document `_id`. 
* Typical SQL relationships (one-many, one-one, many-one) no longer necessary. Instead, references (convention: `collectionname_id`) are made in documents, or data is embedded. If you feel like document size could grow exponentially with embedded data you should use references. 

> When designing a data model, consider how applications will use your database. For instance, if your application only uses recently inserted documents, consider using Capped Collections. Or if your application needs are mainly read operations to a collection, adding indexes to support common queries can improve performance. 

> --[Data Modelling Introduction](http://docs.mongodb.org/manual/core/data-modeling-introduction/)

* Embedded data models allow you to keep things in one place. This is good if data is typically accessed together, potentially bad if it's accessed seperately. Think about your data structure, if embedding causes you to duplicate data in the same document, it's time to use references instead. 
* Indexes can be created for common queries (if you're building a web-app, think "views"). 

* Idea: Particularly useful for CRM software due to lack of requirement for well-formed schema. Easy to contain all information regarding contact or opportunity in one document.


### Importing from CSV

Export from MySQL

    :::sql
    SELECT * INTO OUTFILE '/tmp/table_name.csv'
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    ESCAPED BY '\\' 
    LINES TERMINATED BY '\n'
    FROM table_name
    
Import into MongoDB (see docs on [mongoimport](http://docs.mongodb.org/manual/reference/program/mongoimport/))
    
    :::bash
    mongoimport --collection contacts --headerline --type csv --file Respondents.csv


### When to use MongoDB

Courtesy of [Andre Medeiros](http://staltz.github.io/djangoconfi-mongoengine/#/23)

* You are starting a project with design freedom
* Your app has an evolving data schema
* Your want auto-sharding on a simple schema
* Your app has plenty of geospatial data
* You have high-volume traffic
* Your app works around a RESTful JSON API
