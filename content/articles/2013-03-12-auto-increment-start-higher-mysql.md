title: Forcing Auto Increment to Start Higher in MySQL
date: 2013-03-12
tags: mysql
category: setup
summary: Simple steps to force auto increment to start at a higher number for a primary key in a mysql table

Login to mysql: 

	:::bash
	mysql -u user -p

Choose the database: 

	:::sql
	USE your_db;

Now, we use the `alter` command to adjust auto increment: 

	:::sql
	ALTER TABLE table_name AUTO_INCREMENT=10000;

Where *table_name* is the name of the table you wish to alter. This tells *MySQL* to set the next auto increment to 10000. Be careful, it will overwrite existing records if you
have more than 10k. 
