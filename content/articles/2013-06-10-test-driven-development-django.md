title: Notes on Test Driven Development
category: programming
tags: django, python
author: Brandon Erik Bertelsen
summary: An overview of test driven development with django

Rules of TDD: 

* Test everything. The devil is in the details
* Do not test for more than one condition in the same test
* Do not test constants - they're constant, silly.

