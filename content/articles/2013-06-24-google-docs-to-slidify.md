title: Google Docs to Slidify directly from R
category: programming
tags: Slidify, R, GoogleDocs
author: Brandon Erik Bertelsen
summary: Slidify's only visible weakness is the challenge presented in working on a presentation with other people. These instructions allow you to work with other people on a presentation by using google documents as the host for written content. 

There are a few expectations set out in terms of setting up your document. 

1. `knitr` code chunks must be externalized and identified by `<r-chunk-name>` in your google document
2. Inline code chunks should be identified as `<r-I(code)>` in google documents
3. Script assumes that you have semantic formatting in your google document (ie, header 2 = new slide, bold, italic, underline, and bullets/numbering are all set from the menu - not manual)

Example: 

## Step 1: 

Create a file named `index.Rmd` as below and `gdocs2slidify.r` as below: 

    :::r
    ---
    title : Example Report
    subtitle: Subtitle
    job : Slidify 2013
    mode: standalone # {standalone, draft}
    ---  
    
    {r init, echo=FALSE,message=FALSE, warning = FALSE, cache=FALSE}
    opts_chunk$set(echo=FALSE,
        message=FALSE,
        comment="",
        warning=FALSE,
        results='asis',
        fig.width=12,
        fig.height=5,
        cache=FALSE)
 
    read_chunk('chunks.r')


## Step 2:

Download [html2text.py](https://github.com/aaronsw/html2text/blob/master/html2text.py) and place it in the same directory as index.Rmd

## Step 3: 

    :::r
    library(RGoogleDocs)
    auth = getGoogleAuth("USERNAME", "PASSWORD")
    con = getGoogleDocsConnection(auth) 
    write(file="tmp.html",getDocContent("DOCUMENT_NAME", con))
    system("python html2text.py tmp.html -g -b 0 -i 0 > tmp.rmd")
    
    x <- readChar("tmp.rmd",nchar=100000000)
    x <- gsub("<r-I(.[^>]*)>", "`r I\\1`", x) # find and convert inline <r-I()> code
    x <- gsub("<r-(.[^>]*)>", "\n```{r \\1 } \n```", x) # find and convert r-chunks 
    x <- gsub("  ", "", x) # strange characters from html2text breaks slidify "---"
    x <- gsub("^## |[^#]## ", "\n---\n\n## ", x) # adjust heading level 2 only
 
    write(file="index.Rmd",x, append=TRUE)

### Step 4:

Open index.Rmd (or just wait for it to reload if it's already in RStudio), click "Knit HTML" or `knit2html()`. 