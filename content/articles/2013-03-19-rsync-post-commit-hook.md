title: How to Rsync Files using Post-Commit git Hook
date: 2013-03-19
category: programming
tags: rsync, git
summary: A useful task, is to be able to update a remote server with files once you have commited changes to a git repository. Below are the typical steps used to activate a hook to do so. 

A useful task, is to be able to update a remote server with files once you have commited changes to a git repository. Below are the typical steps used to activate a hook to do so. 

First, navigate to your local repository's hooks folder. 

	:::bash
	cd path/to/project/.git/hooks/

Add a new file called *post-commit* with no extension

	:::bash
	nano post-commit

In that file, copy and paste the following and edit to your needs:

	:::bash
	#!/bin/sh
	rsync -avz --delete --exclude '*.pyc' your/local/path/ user@server.com:/your/remote/path
	
You can remove `--exclude '*.pyc'` or add different exclusions, as I work primarily with python, these files don't need to be moved back and forth.

Now we need to activate the script and make the script executable: 

	:::bash
	chmod +x post-commit 
	
The next time you commit, rsync will synchronize *your/local/path* and *your/remote/path* 