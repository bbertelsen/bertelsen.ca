title: Installing RMySQL Error
category: setup
tags: ubuntu, r
date: 2013-03-21
summary: What to do if you receive the error: could not find the MySQL installation include and/or Library directories when installing RMySQL

If you receive the following error when installing RMySQL: 

	:::bash
	Configuration error:
	  could not find the MySQL installation include and/or library
	  directories.  Manually specify the location of the MySQL
	  libraries and the header files and re-run R CMD INSTALL.
	
	INSTRUCTIONS:
	
	1. Define and export the 2 shell variables PKG_CPPFLAGS and
	   PKG_LIBS to include the directory for header files (*.h)
	   and libraries, for example (using Bourne shell syntax):
	
	      export PKG_CPPFLAGS="-I<MySQL-include-dir>"
	      export PKG_LIBS="-L<MySQL-lib-dir> -lmysqlclient"
	
	   Re-run the R INSTALL command:
	
	      R CMD INSTALL RMySQL_<version>.tar.gz
	
	2. Alternatively, you may pass the configure arguments
	      --with-mysql-dir=<base-dir> (distribution directory)
	   or
	      --with-mysql-inc=<base-inc> (where MySQL header files reside)
	      --with-mysql-lib=<base-lib> (where MySQL libraries reside)
	   in the call to R INSTALL --configure-args='...' 
	
	   R CMD INSTALL --configure-args='--with-mysql-dir=DIR' RMySQL_<version>.tar.gz
	
	ERROR: configuration failed for package ‘RMySQL’
	* removing ‘/home/../R/x86_64-pc-linux-gnu-library/2.15/RMySQL’
	
Open the terminal and install the development libraries for MySQL: 

	:::bash
	sudo apt-get install libdbd-mysql libmysqlclient-dev libmysqlclient18
   
