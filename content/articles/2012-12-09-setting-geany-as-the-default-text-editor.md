title: Setting Geany as the Default Text Editor
category: setup
tags: ubuntu
date: 2012-08-12
summary: On a fresh install of Ubuntu 12.04, the default text editor is gedit. While gedit is a great program, it is not as full featured as Geany. If you'd like to change this, run the following
---

On a fresh install of Ubuntu 12.04, the default text editor is gedit. While gedit is a great program, it is not as full featured as Geany. If you'd like to change this, run the following:

1.  Open a terminal `Ctl + Alt + T`
2.  Type: `sudo cp /usr/bin/geany /usr/bin/gedit` 
3.  Done!

If you want to edit on a per mime-type (read: file type) basis, open `/usr/share/applications/defaults.list` with your preferred text editor and start editing the associations. You can set the defaults to any application that there is `*.desktop` file for in `/usr/share/applications`
