title: Getting Started with Django
category: programming
tags: python, django
date: 2012-12-22
Author: Brandon Erik Bertelsen
summary: Going through the tutorials there are a few elements that are not necessarily obvious for Ubuntu users. In particular, the connector for MySQL and PIP

Going through the tutorials there are a few elements that are not necessarily obvious for Ubuntu users. In particular, the connector for MySQL and PIP

In this document, I organize the things that I installed and document things that will be difficult to remember in the future. 

## Install Needed Libraries (or packages)

	:::bash
	sudo apt-get install python-mysqldb
	sudo apt-get install python-pip
	sudo pip install Django
	sudo pip install django-csvimport 
	sudo pip install django-tastypie 

## Library Information

* [Django documentation](https://docs.djangoproject.com/en/1.4/)
* [csvImporter documentation](http://django-csv-importer.readthedocs.org/en/latest/) This one didn't work

## MySQL Strangeness (Latin vs. UTF8 Errors) 

[Source](http://stackoverflow.com/questions/3513773/change-mysql-default-character-set-to-utf8-in-my-cnf)

Edit my.cnf, on Ubuntu 12.04 that's `sudo nano /etc/mysql/my.cnf`

	:::bash
	[client]
	default-character-set=utf8

	[mysql]
	default-character-set=utf8

	[mysqld]
	collation-server = utf8_unicode_ci
	init-connect='SET NAMES utf8'
	character-set-server = utf8

Then at the command line type: `sudo service mysql restart`
