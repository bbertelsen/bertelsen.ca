title: Connecting Zurmo, SurveyGizmo and ifbyphone
tags: zurmo, ifbyphone, surveygizmo
category: ideas
date: 2012-12-10
Author: Brandon Erik Bertelsen
summary:One of the tasks that I have to work on, and would like to document for the future is the interoperation of Zurmo (CRM software), SurveyGizmo (Survey Software) and ifbyphone (IVR and Call Recoding Software).

One of the tasks that I have to work on, and would like to document for the future is the interoperation of Zurmo (CRM software), SurveyGizmo (Survey Software) and ifbyphone (IVR and Call Recoding Software).

All three are web based services, with the exception of ifbyphone which is also a telephone based medium. 

* Zurmo, is based on PHP and MySQL, and has a really simple seeming API. I'm relying on the simplicity of the API for connecting with the other two pieces of software. [Zurmo API](http://zurmo.org/wiki-subject/api)
* SurveyGizmo can capture URL variables from email and also has callback features that can utilize HTTP POST to pass back information to Zurmo [SurveyGizmo POST Action](http://www.surveygizmo.com/survey-blog/communicate-external-database-http-post/)
* Ifbyphone allows for post call actions, in otherwords after an end-user finishes a call, and the call is marked as complete, ifbyphone will send an HTTP POST request. I'm hoping to use this to pass back information to Zurmo, and to initiate an interaction with SurveyGizmo by emailing the end-user an invitation to participate in a survey. [Ifbyphone Post Call Action](https://secure.ifbyphone.com/developers.php?a=postcall) 
