title: 'Data is' or 'Data are'?
category: research
tags: data
Author: Brandon Erik Bertelsen
date: 2012-12-09

[RE: “Data Is” or “Data Are”?][1]

The Green Plum Data Science blog had a recent article about the use of the word "data" as plural, technically it is plural - but only statisticians seems to use it as such. Google provides two cool research tools that allow us to take a deeper look at the question.

The first is [Google Ngrams][2].

The question posed is whether people are treating the word "data" as plural. In the following graphic courtesy of [google][3], we show the use of "data are" vs. "data is" and "these data" vs. "the data" in something around 5.2 million books (as suggested by [Culturnomics][4]). The results are pretty telling. We see a sharp incline in the use of "the data", with "these data" staying relatively flat and declining after the 90's. Interestingly, we also see an uptick in just at the end of the range. We can see that the bigrams appear very infrequently, but when you're looking at 5.2 million books, even a tiny percentage is statistically useful.

<div class="image">
	<img src=http://books.google.com/ngrams/chart?content=data%20are,data%20is,the%20data,these%20data&corpus=0&smoothing=3&year_start=1800&year_end=2000" />
</div>

The second tool is [Google Search Insights][5].

We can use billions of search queries to *imply* common usage of a number of different terms. The findings are similar. I cut out the word "center" because there it is commonly used in conjunction with the singular form of the noun.

Last but not least, you could always compare raw counts from google search results. In any case, the correct usage of the word is discussed here on the APA Style Guide blog. [Data Is, or Data Are?][6]

 [1]: http://www.greenplum.com/blog/topics/big-data-topics/data-is-or-data-are
 [2]: http://books.google.com/ngrams/
 [3]: http://books.google.com/ngrams/graph?content=data%20are,data%20is,the%20data,these%20data&year_start=1800&year_end=2000&corpus=0&smoothing=3
 [4]: http://www.culturomics.org/Resources/A-users-guide-to-culturomics
 [5]: http://www.google.com/insights/search/
 [6]: http://blog.apastyle.org/apastyle/2012/07/data-is-or-data-are.html
