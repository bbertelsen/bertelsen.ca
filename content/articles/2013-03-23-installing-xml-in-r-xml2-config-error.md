title: Installing XML in R xml2-config error
tags: xml, r
category: setup
date: 2013-03-23
author: Brandon Erik Bertelsen
summary: How to fix the cannot find xml2-config error

There are two ways to fix this error. Either use the XML package from the Ubuntu respository: 

	:::bash
	sudo apt-get install r-cran-xml 
  
Or you can manually install the required library:

	:::bash
	sudo apt-get update
	sudo apt-get install libxml2-dev


  