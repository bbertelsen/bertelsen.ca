title: Ruby vs. Python
tags: python, ruby
category: research
date: 2012-12-10
toc: True
Author: Brandon Erik Bertelsen
summary: With R, I have reached a plateau where solving problems is essentially a trivial matter. Although R provides facility for performing functions outside of data analysis there are a number of elements relating to integration with the web where it simply cannot manage everythihg. I would like to increase the scope of my skills but avoid a low level programming language, so learning Ruby or Python seems to be a logical choice that fits with my career goals.

## Introduction

With R, I have reached a plateau where solving problems is essentially a trivial matter. Although R provides facility for performing functions outside of data analysis there are a number of elements relating to integration with the web where it simply cannot manage everythihg. I would like to increase the scope of my skills but avoid a low level programming language, so learning Ruby or Python seems to be a logical choice that fits with my career goals.

In the following document, we attempt to summarize findings on the differences between Ruby and Python, in an effort to identify which language would best suit my needs in the future. As, in my particular case the goal will to be to convert my analysis into web applications, I will pay particular attention to comparing Ruby on Rails and Django, the competing frameworks for Ruby and Python, respectively.

## Numerical Comparitives

Rather than copy directly from other websites, see the links below: 

* [Infographic Comparitive of PHP, Python and Ruby](http://www.udemy.com/blog/modern-language-wars/)
* [Ranking of Python vs Ruby](http://hammerprinciple.com/therighttool/items/python/ruby)
* [Ranking of Ruby vs Python](http://hammerprinciple.com/therighttool/items/ruby/python)
* [Ranking of all programming languages](http://www.tiobe.com/index.php/content/paperinfo/tpci/index.html)

### Hammerprinciple Statements

When you compare [Ruby vs. Python](http://hammerprinciple.com/therighttool/items/ruby/python) and [Python vs. Ruby](http://hammerprinciple.com/therighttool/items/python/ruby) on the hammerprinciple, the story presented does not seem positive 
for Ruby. Here is an example of the top 5 comparative statements for both. Caution should be taken however, in understanding that the sample size for most measurements is under 200 indivduals. 

<table class="table">
	<tr><th>Python</th><th>Ruby</th></tr>
	<tr><td>This language is good for scientific computer (78%)</td><td>This language is likely to be a passing fad (73%)</td></tr>
	<tr><td>This language is good for numeric computing (78%)</td><td>This language is very flexible (71%)</td></tr>
	<tr><td>Programs written in this language tend to be efficient (76%)</td><td>It is easy to write code that looks like it does one thing but does something else (68%)</td></tr>
	<tr><td>This language is likely to be around for a very long time (76%)</td><td>The semantics of this language are much different than other languages I know (66%)</td></tr>
	<tr><td>This is a mainstream language (73%)</td><td>Code written in this language tends to be terse (65%)</td></tr>
</table>


### Benchmarks

Data Courtesy of: [The Benchmarks Game](http://benchmarksgame.alioth.debian.org/u32/benchmark.php?test=all&lang=python3&lang2=yarv)

In each case below, less it better. 

<div class="image">
	<img src="http://bertelsen.ca/static/assets/research/python-vs-ruby/memory-benchmark.png" alt="Memory Benchmark Ruby vs. Python" />
</div>

Although not an entirely complete set of tests, we can see that there are a number of tasks where Ruby peforms much better than Python and Python better than Ruby. 

<div class="image">
	<img src="http://bertelsen.ca/static/assets/research/python-vs-ruby/cpu-benchmark.png" alt="CPU Benchmark Ruby vs. Python" />
</div>

However, with respect to CPU cycles Python tends to beat Ruby more consistently. 

## Rails vs. Django

Another important piece of the puzzle are the web development frameworks available for both Python and Ruby. There are a number of discussion based posts on stackoverflow that touch on exactly this topic. However, there was one post that particularly pushed me in the direction of Django.

[Source](http://stackoverflow.com/a/92834/170352)

> From my experiences, rails seemed to be more of a black box than django. You issue some command and some stuff happened and you got a basic CRUD application. You run this particular function and something hafappened and you got something else as an output, not knowing what the middle bits were. While this is fine for easy things, the lack of online documentation (in my experience) was a huge hindrance.

> Django, on the other hand, seems to be more logical and transparent about what's going on behind the scenes. There's no "magic". Django is also one of the best documented open source projects out there.

This comment, taken with a number of comments about the relative strength of Django's documentation in comparison to Ruby on Rails was telling. 

There's an interesting [video](http://video.google.com/videoplay?docid=2939556954580527226) on the topic that was also linked in one of the discussions.

## Choice

Given my focus on scientific computing, I already have a significant bent towards Python. In the ranking of Ruby vs. Python above, hammer principle ranks ruby as potentially being a passing fad. I tend to agree. While it is only a small sample of individuals, I think the findings are consistent with opinion based pieces I have read elsewhere on the Internet. Another element that is discussed frequently in articles comparing Ruby and Python was the age, reliability and tools available for the language - in all cases Python was loudly supported for these elements. For these reasons, I'm choosing to learn Python.
 