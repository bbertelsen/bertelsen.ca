title: How to setup error pages with Pelican
date: 2013-03-19
tags: pelican,apache
category: setup
summary: Error pages are a necessary element for any webserver. Pelican's documentation does not provide explicit instructions for how to implement them. However, it's quite simple.

Error pages are a necessary element for any webserver. Pelican's documentation does not provide explicit intructions for how to implement them. However, it's quite simple. 

### Step 1

Create a new page in your page directory. Let's say you entitled it "Error", make sure you add meta to denote that it's a hidden page, as below: 

    :::markdown
    title: Error
    status: Hidden
    
    Your custom error message that extends base.html / page.html
    
### Step 2 

In a text editor create a new document called `.htaccess` and point the errors to the location of your new error file: 

	:::apache
	ErrorDocument 400 /error/
	ErrorDocument 401 /error/
	ErrorDocument 403 /error/
	ErrorDocument 404 /error/
	ErrorDocument 500 /error/
	
Assuming you have pages setup as follows in your Pelican configuration: 

	:::python
	PAGE_URL = '{slug}'
	PAGE_SAVE_AS = '{slug}/index.html'
