title: jQuery Basics
author: Brandon Erik Bertelsen
category: programming
tags: jquery, programming
summary: Some notes about primary jQuery functions

Comments: 

    :::javascript
    // your comment here

Selector Examples:

* "#selector" select by id
* ".selector" select by class
* "p" select by tag
* ".selector.selector2" select by cross class


Always wrap code in this:  

    :::javascript
    $(document).ready(function(){

       // jQuery methods go here...

    });

Or this for shorthand: 

    :::javascript
    $(function(){

       // jQuery methods go here...

    });

Events: 

* Mouse: click, dbclick, mouseenter, mouseleave 
* Keyboard: keypress, keydown, keyup
* Form: submit, change, focus, blur
* Doc/Window: load, resize, scroll, unload

Quicklist of Effect Methods:

* Basic: show(), hide(), toggle(), delay()
* Advanced: slideUp(), slideDown(), fadeIn(), fadeOut(), fadeToggle()

Ajax Methods:

* `$.ajax()`, `$.get()`, `$.post()`, `$.load()`, `ajaxSuccess()`

Debugging in Chrome:

* Push CTRL + Shift + I 
* Or push CTRL + Shift + J 


Addendum:

* Unexpected syntax error on line 1 means you forgot to close `});`


