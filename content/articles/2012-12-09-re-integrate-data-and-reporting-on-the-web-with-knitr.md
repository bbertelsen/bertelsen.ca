title: Integrate data and reporting on the Web with knitr
category: programming 
tags: R, knitr, reporting
date: 2012-12-09
Author: Brandon Erik Bertelsen
summary: I really love the knitr package. It allowed me to convert a wordpress blog into a private knowledge base of survey data and statistical reports with only a few hours of work. It sure beats the hell out of mucking around with latex!

RE: [Revolutions: Integrate data and reporting on the Web with knitr][1].

I really love the knitr package. It allowed me to convert a wordpress blog into a private knowledge base of survey data and statistical reports with only a few hours of work. It sure beats the hell out of mucking around with latex!

Fantastic tool knitr is!

With respect to documentation. I really hope r-dev pick this up. It would be great to see exhaustive statistical examples included in base R documentation (and make it easier for package authors to do so). I wonder if Hadley is considering knitr in his redesign of Roxygen3...

 [1]: http://blog.revolutionanalytics.com/2012/09/data-reporting-knitr.html
