title: Install ownCloud on Ubuntu
tags: owncloud, ubuntu
category: setup
date: 2013-04-13
author: Brandon Erik Bertelsen
summary: Getting started with ownCloud on Ubuntu

## Install ownCloud

Open a terminal, and edit your sources 

    sudo nano /etc/apt/sources.list
Copy and paste the following:

    deb http://download.opensuse.org/repositories/isv:ownCloud:community/xUbuntu_12.10/ /

Add the repository key: 

    wget http://download.opensuse.org/repositories/isv:ownCloud:community/xUbuntu_12.10/Release.key
    sudo apt-add - < Release.key
    
Update, and install

    sudo apt-get update
    sudo apt-get install owncloud




