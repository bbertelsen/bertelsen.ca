Title: An Introduction to Reproducible Research with R
Date: 2012-12-09
Tags: R, reproducible, latex
category: programming
Author: Brandon Erik Bertelsen
Summary: The concept is simple, provide a set of files that anyone can use to recreate your output in exactly the same fashion that you have. [Reproducible Research][1] is not new. The ability to look back and say "here is how we did that" - is worth the extra time required to do so.

The concept is simple, provide a set of files that anyone can use to recreate your output in exactly the same fashion that you have. [Reproducible Research][1] is not new. The ability to look back and say "here is how we did that" - is worth the extra time required to do so. R is an effective way to generate large amounts of customized reporting in a very short time span, with limited room for error (with a bit of forethought) and a relatively small amount of time. This concept can be extended to generating reports for large distributions or even more customized or exploratory reporting.

With R this is quite simple. The most common method used is to create an `*.Rnw` file that loads all of your libraries, data and calculations within. Sharing this file makes it easy for others to simply reproduce your output and regenerate your code. Below is a really simple example of the results of a linear regression on auto-generated data. For the time being, we will not go into much detail about [Sweave][2], but you should know - it's the magic behind getting your R code into a format that can be converted into PDF.

Here are just a few usage case scenarios

*   High quality, corporate themed articles and reports that are easy to duplicate or recreate
*   Customized reports, for business intelligence, marketing research, or data mining exercises
*   Reporting on financial data, or metrics.
*   Human readable tables for model selection or parameter interpretation
*   Exploratory data analysis: plot and tabulate everything, interpret later

The real challenge that surrounds reproducible research with R, is learning LaTex. It's an old format and there are quite a few peculiarities that you will encounter that are neither immediately obvious nor intuitive. Let's keep this in mind.

However, there are a few really useful places for information on using LaTex:

1.  [Introduction to Latex][3] Before you begin, you **must** read it and follow the installation instructions for your operating system. It is an excellent source of information for a beginner and tackles many of the frequently asked questions that would generally crush one's soul otherwise
2.  [Tex Stackexchange][4] is where you go if your problem is more specific to LaTeX, you can always get a quick answer from real people there
3.  And of course, there's [Stack Overflow][5] for questions pertaining to R

Without further ado, let's create a small piece of reproducible research

*   Here is the [Rnw file][6]
*   And here is the [resulting PDF][7]

Now, it's a fairly simple matter of setting your working directory and running a few more lines of code in R. If you're using [RStudio][8] (which I highly recommend) you can simply open the Rnw file and click on "CompilePDF" on the toolbar. From the console:

	:::r
	setwd('path/to/Example.Rnw/')
	Sweave('Example.Rnw')
	texi2pdf('Example.tex')
	  

That's a wrap.

 [1]: http://en.wikipedia.org/wiki/Reproducibility#Reproducible_research "Reproducible Research on Wikipedia"
 [2]: http://www.statistik.lmu.de/~leisch/Sweave/ "Sweave Introduction"
 [3]: http://en.wikibooks.org/wiki/LaTeX/Introduction "Latex Intro"
 [4]: http://tex.stackexchange.com/ "Tex Stackexchange Q&A"
 [5]: http://stackoverflow.com "Stack Overflow"
 [6]: http://bertelsen.ca/wp-content/uploads/2012/02/Example.Rnw "Example Rnw file"
 [7]: http://bertelsen.ca/wp-content/uploads/2012/02/Example.pdf "Example PDF file"
 [8]: http://rstudio.org "RStudio IDE for R"
