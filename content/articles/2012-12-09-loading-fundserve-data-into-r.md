title: Scraping tables from the web
category: programming
tags: r
date: 2012-11-02
Author: Brandon Erik Bertelsen
summary: If, like me, you do a bit of work with mutual fund data across a number of sources, you'll eventually need to pull in the dealer codes or the fund codes. Below is a working example of how to pull in active dealership codes

If, like me, you do a bit of work with mutual fund data across a number of sources, you'll eventually need to pull in the dealer codes or the fund codes. Below is a working example of how to pull in active dealership codes

	:::r
	library(XML)
	theurl <- "http://goo.gl/2X5LK"
	dealer.codes <- readHTMLTable(theurl)
	n.rows <- unlist(lapply(dealer.codes, function(t) dim(t)[1]))
	dealer.codes <- dealer.codes[[which.max(n.rows)]]

Minor adjustments of the method above can be used to pull any most types of tabular data from a webpage to R.
