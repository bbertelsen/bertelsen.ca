title: Extend Battery Ubuntu 12.04 on Dell XPS 15z
category: setup
summary: When I loaded a fresh install of Ubuntu on my Dell XPS 15, the initial battery life was *horrible*, no - *terrible*. A measly 90 minutes of battery life, to begin with. The problem is related to the drivers for the NVIDIA Optimus graphics card. Unfortunately, NVIDIA is somewhat well known for having a [bad relationship][1] with Linux.
tags: ubuntu
date: 2012-12-09

When I loaded a fresh install of Ubuntu on my Dell XPS 15, the initial battery life was *horrible*, no - *terrible*. A measly 90 minutes of battery life, to begin with. The problem is related to the drivers for the NVIDIA Optimus graphics card. Unfortunately, NVIDIA is somewhat well known for having a [bad relationship][1] with Linux.

[Bumblebee][2] to the rescue!

*For 12.04:*


	:::bash
	sudo add-apt-repository ppa:bumblebee/stable


*For 11.04 and Older (10,9,8...etc)*

	:::bash
	sudo add-apt-repository ppa:ubuntu-x-swat/x-updates
    

Then:

	:::bash
	sudo apt-get update
	sudo apt-get install bumblebee bumblebee-nvidia

Even without re-logging, I watched my battery time go from "1:25" to "3:10".

 [1]: http://www.youtube.com/watch?v=_36yNWw_07g
 [2]: https://wiki.ubuntu.com/Bumblebee
