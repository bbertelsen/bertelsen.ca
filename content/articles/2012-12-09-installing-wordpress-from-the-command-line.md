title: Installing wordpress from the command line
summary: How to install wordpress over SSH (the unix command line). Personally, I'm using Ubuntu 12.04 LTS, so this is formatted accordingly 
category: setup
tags: wordpress
date: 2012-05-09

How to install wordpress over SSH (the unix command line). Personally, I'm using Ubuntu 12.04 LTS, so this is formatted accordingly

## Step 1

Create a MySQL database. Replace `database_name`, with the name of the new database.

	:::bash
	mysql -u -root -p
	create database database_name; 
	
    
## Step 2

Download and unpack the latest Wordpress distribution.

	:::bash
	cd /home/user/public/example.com/public/
	wget http://wordpress.org/latest.tar.gz  
	tar xfz latest.tar.gz
	cp -R wordpress/* ./
	rm -rf wordpress/ latest.tar.gz

## Step 3

Now, edit the configuration file and you're live!
