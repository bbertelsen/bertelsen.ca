title: About

Brandon is an experienced research consultant who has a focus on solving business problems through statistical analysis, business process automation and web application development. Making sense of data with R, Python and the web are his primary interests.

Brandon has a BComm in Economics and Management Science from Ryerson University. When not otherwise engaged in work, Brandon spends nearly all of his time on continuing education, tinkering with technology, and furthering his personal goal of becoming a polyglot. He also enjoys volunteering as a math and science tutor with [Pathways to Education](http://www.regentparkchc.org/youth-programs/pathways-to-education).

<hr>
<p class="pull-right"><a href="http://linkedin.com/in/bbertelsen"><img style="opacity:0.5" width="30px" src="http://bertelsen.ca/static/assets/icons/linkedin.png" alt="Brandon on Linkedin" /></a>
<a href="http://twitter.com/1beb"><img style="opacity:0.5" width="30px" src="http://bertelsen.ca/static/assets/icons/twitter.png" alt="Brandon on Twitter" /></a>
<a href="http://plus.google.com/u/0/117173181056717097296?rel=author"><img style="opacity:0.5" width="30px" src="http://bertelsen.ca/static/assets/icons/googleplus.png" alt="Brandon on Google+"/></a>
<a href="https://github.com/1beb"><img style="opacity:0.5" width="30px" src="http://bertelsen.ca/static/assets/icons/github.png" alt="Brandon on Google+"/></a>
<a href="http://stackexchange.com/users/56876">
<img src="http://stackexchange.com/users/flair/56876.png?theme=clean" width="208" height="58" alt="profile for Brandon Bertelsen on Stack Exchange, a network of free, community-driven Q&amp;A sites" title="profile for Brandon Bertelsen on Stack Exchange, a network of free, community-driven Q&amp;A sites">
</a></p> 

<p>
Brandon Erik Bertelsen<br/>
Toronto, ON M5A 3X1<br/>
c: 647-287-1009<br/>
o: 647-722-0481<br/>
e: brandon@bertelsen.ca<br/>
skype: brandon.bertelsen<br/></p>
<hr>


## Purpose 

This is not a blog. Lately, I've been feeling as though documenting my work, research, and ideas have become 
a largely painful activity with my thoughts spread across notebooks, blogs, text files, and buried in research 
papers. That ends here. The aim of this site is to create a static knowledge base of all the things I've been working on and 
processes I'm likely to forget, taking inspiration from [Carl Boettiger](http://www.carlboettiger.info/2012/09/28/Welcome-to-my-lab-notebook.html).

## Technical Details

This site is hosted on a [linode](http://linode.com) VPS, is generated using [Pelican](http://getpelican.com) and uses a modified version of [simple-bootstrap](https://github.com/getpelican/pelican-themes/tree/master/simple-bootstrap). This linode VPS runs on [Ubuntu 12.04](http://www.ubuntu.com/download). Social
icons courtesy of [Pixeden](http://www.pixeden.com/social-icons/simple-social-icons-vol1)

## Disclaimer

Information on this website is provided 'AS IS' without warranty of any kind, express or implied.  Information 
on this website is offered in good faith, it does not constitute legal nor financial advice.The opinions 
expressed on this wesbite are the sole expression of Brandon Bertelsen and do not represent any other entity 
that he may be affiliated with.

  