title: 404 Error
slug: error
status: hidden

## That's Embarrasing

If you feel the cold pang of loss, rejoice in the knowledge that you've had something that was worth 
losing. With that in mind, whatever you were looking for no longer exists. My bad.

