title: Survey Analysis Package for R
slug: surv2
status: hidden
toc: True

The *surv2* package for the R programming language is designed to facilitate: processing, 
summarizing, crosstabulating and plotting marketing research data. The packages utilizes the questionnaire that the survey data is based on.

## Motivation

When I first started using R. I needed a package that would always return data in the form of a 
*data.frame* for presentation and export purposes. None of the current tools do so in an easy and useful way.
While, `table`, `xtabs`, and `prop.table` are incredibly useful tools for analysis that doesn't need to 
be reproduced, they are not easily shared nor particularly useful as output formats in the typical market research 
project workflow. Clients, will not accept text files of unstructured data that they can not utilize with the productivity
software that they are used to. (read: Microsoft Office &copy;) 

## Survey Data Paradigm

Survey data are always in the form of a *data.frame*. In almost all situations, the questionnaire
provides all of the information necessary to pull or manipulate data into meaningful parts. Generally, responses to questions fall 
into five categories.

1. Categorical, single response (e.g, Yes/No, Likert)
2. Categorical, multiple response (e.g, Likert response to statements, personality, multipunch questions)
3. Continuous, single response (e.g, Year of birth, Frequency of visit)
4. Continuous, multiple response (e.g, Estimated proportions, Shopping purchases)
5. Verbatim (e.g, Raw text)

This means that survey data exhibits basic characteristics that allow us to utilize a somewhat automated method to 
classify it in a manner that is useful for further analysis.

## Typical Workflow

The questionnaire is the key point of definition for all data collected and it is also the starting point for 
all analysis via this package. The following is a typical workflow pattern associated with using the *surv2* package.

1. Import your data into R (`read.csv`, `read.spss` from the foreign package, etc)
2. Convert the questionnaire to a text document.
3. Recode the survey data by wrapping the questionnaire text with appropriate *surv2* functions like `surv.rc` and `surv.pull`.
4. Save all of the recoded question data in a list. 
5. Summarize data using `lapply(question_data, surv.s)`
6. Review and plot basic question statistics as necessary
7. Crosstabulate questions as necessary `surv.csub(question1,question2)`
8. Review and plot crosstabulation statistics as necessary

A 20 minute survey takes about 2-4 hours to have full statistics for, depending on the complexity of the questionnaire and another 
3-4 hours to develop reporting for. 

## Full Example

The following example presents the process from start to finish. Beginning with a questionnaire and ending with a report. All 
of the files necessary to reproduce this example are available in the list below

* Questionnaire
* Recoding file
* Summarization
* Crosstabulation
* Example knitr report

### Example Questionnaire

Let's say you have a basic questionnaire with four questions: 

Q1. Do you agree to participate in this research?

* Yes
* No

Q2. How concerned are you about each of the following...?

Responses 

* Very concerned
* Concerned
* Somewhat concerned
* Mildly concerned
* Not at all concerned

Statements

* The environment
* The state of the economy

Q3. Please enter your year of birth...

Q4. Please estimate the proportion of your time you spend...?
	
Statements

* On the Internet
* On the phone
* Using a mobile device

### Example Data

	:::r
	survey_data <- data.frame(
  		q1=sample(1:2,1000,replace=TRUE),
  		q2_1=sample(1:5,1000,replace=TRUE),
  		q2_2=sample(1:5,1000,replace=TRUE),
  		q3_1=sample(1950:1995,1000,replace=TRUE),
  		q4_1=runif(1000)*100,
  		q4_2=runif(1000)*100,
  		q4_3=runif(1000)*100
  		)

### Recoding

Now we have *survey_data* in our workspace, let's use it to recode some of our data into a format that's more useful for our 
analysis.

First, let's start by initializing a list to hold our question data:

	:::r 
	q2013 <- list()
	
Now, let's look at the pattern of column names of the data, so we ensure that we're pulling the right data for the right question:

	:::r
	> names(survey_data)
	[1] "q1"   "q2_1" "q2_2" "q3_1" "q4_1" "q4_2" "q4_3"

A categorical question, with only one response:

	:::r
	q2013$participate <- surv.rc(surv.pull(survey_data, "q1"),"Q1. Do you agree to participate in this research?
  	Yes
	No")

A categorical question, with multiple responses:
	
	:::r
	q2013$concerns <- surv.rc(surv.pull(survey_data, "q2"),"Q2. How concerned are you about each of the following:
  	Very Concerned
	Concerned
	Somewhat concerned
	Mildly concerned
	Not at all concerned",
  	"The environment
	The state of the economy")
	
A continuous question, with a single response:

	:::r
	q2013$birthday <- surv.rc(surv.pull(survey_data, "q3"),"Q3. Please enter your year of birth")

A continuous question, with multiple responses: 

	:::r
	q2013$time <- surv.rc(surv.pull(survey_data, "q4"),"Q4. Please estimate the proportion of your time you spend...",
  	"On the Internet
	On the phone
	Using a mobile device")

Verbatim questions have no special recoding funciton as they are not meant to be recoded (in the same sense). However,
one could use the `surv.pull` function to capture them if they all had common strings in the column
names. For example: 

	:::r
	verbs <- surv.pull("other",survey_data) # empty in this example
	
It's generally best practice to keep un-recoded data in a seperate list so as to avoid problems when you use `apply`.

### Summarizing Data

Now that we have recoded question data, let's start reviewing some of our data. We can quickly and easily summarize our 
data into meaningful summaries that allow for nearly instantaneous inference to be drawn.  

#### Categorical 

	:::r
	> surv.s(q2013$time) # categorical 
	  Response Frequency Proportion
	1      Yes       498         50
	2       No       502         50

#### Categorical Statements

	:::r
	> surv.s(q2013$concerns) 
	                  Statement             Response Frequency Proportion    n
	1           The environment       Very Concerned       189         19 1000
	2           The environment            Concerned       173         17 1000
	3           The environment   Somewhat concerned       211         21 1000
	4           The environment     Mildly concerned       201         20 1000
	5           The environment Not at all concerned       226         23 1000
	6  The state of the economy       Very Concerned       196         20 1000
	7  The state of the economy            Concerned       203         20 1000
	8  The state of the economy   Somewhat concerned       202         20 1000
	9  The state of the economy     Mildly concerned       189         19 1000
	10 The state of the economy Not at all concerned       210         21 1000

#### Categorical Statement Detail

	:::r
	> lapply(q2013$concerns,surv.s)
	$`The environment`
	              Response Frequency Proportion
	1       Very Concerned       189         19
	2            Concerned       173         17
	3   Somewhat concerned       211         21
	4     Mildly concerned       201         20
	5 Not at all concerned       226         23

	$`The state of the economy`
	              Response Frequency Proportion
	1       Very Concerned       196         20
	2            Concerned       203         20
	3   Somewhat concerned       202         20
	4     Mildly concerned       189         19
	5 Not at all concerned       210         21	
	
#### Continuous
	
	:::r
	> surv.s(2013 - q2013$birthday ) # continuous
  	  Response  Mean    SD 25th 50th 75th 100th Min Max    n
	1 Response 40.57 13.08   30   40   52    63  18  63 1000
	
#### Continuous Statements

	:::r
	> surv.s(q2013$time)
	               Response  Mean    SD  25th  50th  75th  100th  Min    Max    n
	1       On the Internet 49.52 29.04 24.18 50.67 75.13 100.00 0.08 100.00 1000
	2          On the phone 49.22 28.52 24.81 48.63 74.06  99.98 0.30  99.98 1000
	3 Using a mobile device 50.96 28.85 26.78 51.38 76.37  99.93 0.27  99.93 1000

### Crosstabulation

### Ploting

## Release Information

I have not yet decided whether or not I wish to submit this package to CRAN. The additional functionality it provides over 
existing built in functions is minimal. 

### Function Descriptions

#### surv.pull

This function pulls in questions matching a specific pattern. There are a few tricks that we use here. An example 
pattern `"[q][1][.]."` pulls in columns matching `q1.*`

#### surv.rc

This function recodes survey data and adds appropriate attributes, preparing data for analysis. 

#### surv.c

Classifies questionnaire data as categorical or continuous automatically. Adds other
useful classification elements for summarization and crosstabulation purposes.

#### surv.aqa and surv.sqa

surv.sqa: This utility function saves attributes for question data. It is primarily used to carry 
question attributes forward through transformations and summarizations

surv.aqa: This utility function applies attributes for question data. It is primarily used to carry 
question attributes forward through transformations and summarizations

#### surv.s

This function creates summaries from any type of data: vectors, factors or data frames

#### surv.csub

This function provides a side by side summary and comparison of subsets of the same data set. It 
takes a survey question, and cross tabulates on the basis of some subset variable