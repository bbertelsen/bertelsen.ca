title: RGoogleDrive
slug: rgoogledrive
status: hidden
toc: False

This R package is largely a wrapper for Hadley Wickham's [httr](https://github.com/hadley/httr), built with the specific intention of providing access to the [Google Drive SDK](https://developers.google.com/drive/v2/reference/) from within R.

Google's updated API relies on OAuth 2.0 making it impossible to use simple authentication past the deprecation date of the first API (Scheduled for April
2015). Unfortunately, this means that [RGoogleDocs](http://www.omegahat.org/RGoogleDocs/) will no longer work after this date. 

Please visit the github page for the project for more information: [RGoogleDrive on github](https://github.com/1beb/RGoogleDrive)