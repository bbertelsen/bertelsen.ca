title: Projects

Unfortunately the vast majority of my work is proprietary, but here are a few open source projects that I authored. 

* [RGoogleDrive](http://bertelsen.ca/rgoogledrive) : An R package that provides to Google Drive files through the V2 API from within R

